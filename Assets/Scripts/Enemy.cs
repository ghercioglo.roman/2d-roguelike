﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MovingObject {

    public int playerDamage;
    private Animator animator;
    private Transform target;
    private bool skipMove;
    public float turnTime = 0.05f;

    public AudioClip enemyAttackSound1;
    public AudioClip enemyAttackSound2;

    protected override void Start()
    {
        GameManager.getInstance().addEnemyToList(this);
        animator = GetComponent<Animator>();
        target = GameObject.FindGameObjectWithTag("Player").transform;
        base.Start();
    }

    protected override void attemptMove<T>(int xDir, int yDir)
    {
        if (skipMove)
        {
            skipMove = false;
            return;
        }

        base.attemptMove<T>(xDir, yDir);
        skipMove = true;
    }

    public void moveEnemy()
    {
        int xDir = 0;
        int yDir = 0;

        if(Mathf.Abs(transform.position.x - target.position.x) < float.Epsilon)
        {
            yDir = target.position.y > transform.position.y ? 1 : -1;
        }
        else
        {
            xDir = target.position.x > transform.position.x  ? 1 : -1;
        }

        attemptMove<Player>(xDir, yDir);
    }

    protected override void onCantMove<T>(T component)
    {
        Player hitPlayer = component as Player;
        animator.SetTrigger("enemyAttack");

        SoundManager.getInstance().randomizeSfx(enemyAttackSound1, enemyAttackSound2);

        hitPlayer.loseFood(playerDamage);
    }

}
