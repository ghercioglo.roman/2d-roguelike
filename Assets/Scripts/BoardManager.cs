﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;

public class BoardManager : MonoBehaviour {
    public GameObject exit;
    public GameObject[] outerWallTiles;
    public GameObject[] wallTiles;
    public GameObject[] floorTiles;
    public GameObject[] foodTiles;
    public GameObject[] enemyTiles;
    public int nrColumns = 8;
    public int nrRows = 8;
    public Count wallCount = new Count(5, 9);
    public Count foodCount = new Count(1, 5);
    private Transform boardHolder;
    private List<Vector3> gridPositions = new List<Vector3>();

    [Serializable]
    public class Count
    {
        public int min;
        public int max;
        public Count(int min, int max) {
            this.min = min;
            this.max = max;
        }
    }

    //get all possible tile positions 
    void initializeList()
    {
        gridPositions.Clear();
        for(int x = 1; x < nrColumns-1; ++x)
        {
            for (int y = 1; y < nrRows - 1; ++y)
            {
                gridPositions.Add(new Vector3(x, y, 0.0f));
            }
        }
    }

    //setup floor and borders
    void setupBoard()
    {
        boardHolder = new GameObject("Board").transform;
        for (int x = -1; x < nrColumns + 1; ++x) {
            for (int y = -1; y < nrRows + 1; ++y)
            {
                GameObject toInstatiate = floorTiles[Random.Range(0, floorTiles.Length)];
                if(x==-1 || x == nrColumns || y==-1 || y == nrRows)
                    toInstatiate = outerWallTiles[Random.Range(0, outerWallTiles.Length)];
                GameObject instance = Instantiate(toInstatiate, new Vector3(x, y, 0.0f), Quaternion.identity, boardHolder);
            }
        }
    }

    Vector3 randomPosition()
    {
        int randomIndex = Random.Range(0, gridPositions.Count);
        Vector3 position = gridPositions[randomIndex];
        gridPositions.RemoveAt(randomIndex);
        return position;
    }

    void layoutObjectAtRandom(GameObject[] tileList, int min, int max)
    {
        int objectCount = Random.Range(min, max + 1);
        for (int a=0; a < objectCount; ++a)
        {
            Vector3 position = randomPosition();
            GameObject chosedTile = tileList[Random.Range(0, tileList.Length)];
            Instantiate(chosedTile, position, Quaternion.identity, boardHolder);
        }
    }

    public void setupScene(int level)
    {
        setupBoard();
        initializeList();
        layoutObjectAtRandom(wallTiles, wallCount.min, wallCount.max);
        layoutObjectAtRandom(foodTiles, foodCount.min, foodCount.max);
        int nrEnemy = (int)Mathf.Log(Mathf.Clamp(level*2, 3, 100), 2);
        layoutObjectAtRandom(enemyTiles, nrEnemy, nrEnemy);
        Instantiate(exit, new Vector3(nrColumns - 1, nrRows - 1, 0.0f), Quaternion.identity, boardHolder);
    }
}
