﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class MovingObject : MonoBehaviour {
    public float moveTime = 0.1f;
    public LayerMask blockingLayer;
    private Rigidbody2D rigidBody;
    private BoxCollider2D boxCollider;
    private float inverseMoveTime;

    protected virtual void Start()
    {
        rigidBody = GetComponent<Rigidbody2D>();
        boxCollider = GetComponent<BoxCollider2D>();
        inverseMoveTime = 1.0f / moveTime;
    }

   
    protected IEnumerator smoothMovement(Vector3 end)
    {
        float sqrRemainingDistance = (transform.position - end).sqrMagnitude;

        while(sqrRemainingDistance > float.Epsilon)
        {
            Vector3 newPosition = Vector3.MoveTowards(rigidBody.position, end, inverseMoveTime * Time.deltaTime);
           // print("NewPos: " + newPosition);
            rigidBody.MovePosition(newPosition);
            sqrRemainingDistance = (transform.position - end).sqrMagnitude;
            yield return null;
        }

        StopAllCoroutines();
    }


    protected bool move(int xDir, int yDir, out RaycastHit2D hit)
    {
        Vector2 start = rigidBody.position;
        Vector2 end = start + new Vector2(xDir, yDir);
       // print("Start vec2: " + start);
        //print("End vec2: " + end);

        boxCollider.enabled = false;
        hit = Physics2D.Linecast(start, end, blockingLayer);
        boxCollider.enabled = true;

        if(hit.transform==null)
        {
           // print("Hit transform null");
            StartCoroutine(smoothMovement(end));
            return true;
        }

        return false;
    }

    protected abstract void onCantMove<T>(T component) where T : Component;
    protected virtual void attemptMove<T>(int xDir, int yDir) where T : Component
    {
        RaycastHit2D hit;
        bool canMove = move(xDir, yDir, out hit);

        if (hit.transform == null)
            return;

        T hitComponent = hit.transform.GetComponent<T>();

        if (!canMove && hitComponent != null)
            onCantMove(hitComponent);
    }
}
