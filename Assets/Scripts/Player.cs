﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Player : MovingObject {
    //vars
    public int wallDamage = 1;
    public int pointsPerFood = 10;
    public int pointsPerSoda = 20;
    public float restartLevelDelay = 1.0f;
    public int food;
    //GUI
    public Text foodText;
    //Audio
    public AudioClip moveSound1;
    public AudioClip moveSound2;
    public AudioClip eatSound1;
    public AudioClip eatSound2;
    public AudioClip drinkSound1;
    public AudioClip drinkSound2;
    public AudioClip gameOverSound;
    //other
    private Vector3 touchOrigin;
    public Animator animator;


    protected override void Start () {
        animator = GetComponent<Animator>();
        food = GameManager.getInstance().playerFoodPoint;

        foodText.text = "Food: " + food;

        base.Start();
	}

    private void OnDisable()
    {
        GameManager.getInstance().playerFoodPoint = food;
    }

    // Update is called once per frame
    void Update()
    {
        if (!GameManager.getInstance().playersTurn) return;

        int horizontal = 0;
        int vertical = 0;

#if UNITY_EDITOR || UNITY_STANDALONE || UNITY_WEBPLAYER
        horizontal = (int)Input.GetAxisRaw("Horizontal");
        vertical = (int)Input.GetAxisRaw("Vertical");
        if (horizontal != 0)
        {
            vertical = 0;
        }
#else
        touchOrigin=-Vector3.one;
        if (Input.touchCount > 0)
        {
            Touch touch = Input.touches[0];
            if(touch.phase == TouchPhase.Began)
            {
                touchOrigin = touch.position;
            }
            else (touch.phase == TouchPhase.Ended && touchOrigin.x >= 0)
            {
                Vector3 touchEnd = touch.position;
                float x = touchEnd.x - touchOrigin.x;
                float y = touchEnd.y - touchOrigin.y;
                touchOrigin = -Vector3.one;

                if (Mathf.Abs(x) > Mathf.Abs(y))
                    horizontal = x > 0 ? 1 : -1;
                else
                    vertical = y > 0 ? 1 : -1;
            }
        }      
#endif

        if (horizontal != 0 || vertical != 0)
        {
            attemptMove<Wall>(horizontal, vertical);
        }
    }

    protected override void onCantMove<T>(T component)
    {
        Wall hitWall = component as Wall;
        hitWall.damageWall(wallDamage);
        animator.SetTrigger("playerChop");
    }

    public void loseFood(int dmg)
    {
        foodText.text = "-" + dmg + " Food: " + food;

        animator.SetTrigger("playerHit");
        food -= dmg;
        checkIfGameOver();
    }

    private void checkIfGameOver()
    {
        if (food <= 0)
        {
            SoundManager.getInstance().playSingle(gameOverSound);
            SoundManager.getInstance().musicSource.Stop();
            GameManager.getInstance().gameOver();
        }
    }

    protected override void attemptMove<T>(int xDir, int yDir)
    {
        --food;
        foodText.text = "Food: " + food;

        base.attemptMove<T>(xDir, yDir);

        RaycastHit2D hit;
        if(move(xDir,yDir, out hit)){
            SoundManager.getInstance().randomizeSfx(moveSound1, moveSound2);
        }

        checkIfGameOver();
        GameManager.getInstance().playersTurn = false;
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "Exit")
        {
            if (!GameManager.getInstance().restarting)
            {
                GameManager.getInstance().restarting = true;
                GameManager.getInstance().IncreaseLevel();
            }

        }
        else if (collider.tag == "Food")
        {
            food += pointsPerFood;
            foodText.text = "+" + pointsPerFood + " Food: " + food;
            SoundManager.getInstance().randomizeSfx(eatSound1, eatSound2);
            collider.gameObject.SetActive(false);
        }
        else if (collider.tag == "Soda")
        {
            food += pointsPerSoda;
            foodText.text = "+" + pointsPerSoda + " Food: " + food;
            SoundManager.getInstance().randomizeSfx(drinkSound1, drinkSound2);
            collider.gameObject.SetActive(false);
        }
    }
}
