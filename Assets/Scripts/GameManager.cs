﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;



public class GameManager : MonoBehaviour {
    private static GameManager instance = null;
    public BoardManager boardManager;
    //Game
    private int level = 0;
    public bool playersTurn = true;
    public int playerFoodPoint = 100;
    private bool enemiesMoving;
    private float turnDelay = 0.05f;
    private List<Enemy> enemies;
    //GUI
    private Text levelText;
    private GameObject levelImage;
    private bool doingSetup;
    public float levelStartDelay = 2.0f;
    internal bool restarting = false;

    public static GameManager getInstance() { return instance; }
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this);
        }else if(instance!=this)
        {
            DestroyImmediate(gameObject);
            return;
        }

        enemies = new List<Enemy>();
        boardManager = GetComponent<BoardManager>();
        OnLevelWasLoaded(-1);
    }

    void Update()
    {
        if (playersTurn || enemiesMoving || doingSetup)
            return;

        StartCoroutine(moveEnemies());
    }

    private void OnLevelWasLoaded(int index)
    {
        level++;
      //  Debug.LogError("Current level: " + level);
        initLevel();
    }
   

    public void addEnemyToList(Enemy enemy)
    {
        enemies.Add(enemy);
    }

    //---------------
    public void gameOver()
    {
        levelText.text = "After " + level + " days, you starved.";
        levelImage.SetActive(true);

        enabled = false;
    }

    private void initLevel()
    {
        doingSetup = true;

        levelImage = GameObject.Find("Image");
        levelText = GameObject.Find("DayText").GetComponent<Text>();

        levelImage.SetActive(true);
        levelText.text = "Day " + level;
        Invoke("hideLevelImage", levelStartDelay);

        enemies.Clear();

        boardManager.setupScene(level);

        restarting = false;
    }

    private void hideLevelImage()
    {
        levelImage.SetActive(false);
        doingSetup = false;
    }

    
    public void IncreaseLevel()
    {
        SceneManager.LoadScene(0);
    }

    IEnumerator moveEnemies()
    {
        enemiesMoving = true;
        yield return new WaitForSeconds(turnDelay);
        if(enemies.Count == 0)
            yield return new WaitForSeconds(turnDelay);

        for(int a=0; a < enemies.Count; ++a)
        {
            enemies[a].moveEnemy();
            yield return new WaitForSeconds(enemies[a].turnTime);
        }
        playersTurn = true;
        enemiesMoving = false;
    }
}
