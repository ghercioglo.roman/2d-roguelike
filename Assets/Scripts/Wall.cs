﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wall : MonoBehaviour {
    public int hp = 4;
    public SpriteRenderer spriteRenderer;
    public Sprite damageSprite;

    public AudioClip wallDamagedSound1;
    public AudioClip wallDamagedSound2;

    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    public void damageWall(int dmg)
    {
        spriteRenderer.sprite = damageSprite;
        hp -= dmg;

        SoundManager.getInstance().randomizeSfx(wallDamagedSound1, wallDamagedSound2);

        if (hp <= 0)
            gameObject.SetActive(false);
    }
	
}
